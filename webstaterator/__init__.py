"""
Webstaterator

A Python tool for generating static websites based on object models

Documentation: https://gitlab.com/Jon.Keatley.Folio/webstaterator
Gitlab: https://gitlab.com/Jon.Keatley.Folio/webstaterator
PyPi: https://pypi.org/project/webstaterator/

Created by Jon Keatley (http://jon-keatley.com)
Named by Sasha Siegel. It is her fault!

Copyright Jon Keatley 2021

"""
